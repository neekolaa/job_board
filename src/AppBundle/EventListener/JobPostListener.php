<?php

namespace AppBundle\EventListener;

use Doctrine\Common\Persistence\ObjectRepository;
use FOS\UserBundle\Model\UserInterface;
use AppBundle\Event\FirstJobPostedEvent; 
use AppBundle\Entity\Job;

class JobPostListener 
{
    /**
     *
     * @var \Twig_Environment 
     */
    protected $twig;
    
    /**
     *
     * @var \Swift_Mailer 
     */
    protected $mailer;
    
    /**
     *
     * @var ObjectRepository
     */
    protected $userReposistory;
    
    public function __construct(\Twig_Environment $twig, \Swift_Mailer $mailer, ObjectRepository $userReposistory) 
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->userReposistory = $userReposistory;
    }
    
    /**
     * 
     * @param FirstJobPostedEvent $event
     */
    public function onFirstJobPosted(FirstJobPostedEvent $event) 
    {
        $job = $event->getJob();
        
        $moderators = $this->userReposistory->findByRole('ROLE_MODERATOR');
        
        foreach ($moderators as $moderator) {
            $message = $this->createMessage($job, $moderator);
            $this->mailer->send($message);
        }
        
    }
    
    /**
     * 
     * @param Job $job
     * @param UserInterface $moderator
     * @return Swift_Message
     */
    private function createMessage(Job $job, UserInterface $moderator) 
    {
        $mailBody = $this->renderModeratorTemplate($job, $moderator);
        
        return \Swift_Message::newInstance()
            ->setSubject('First Job post: ' . $job-getTitle() . '/' . $job->getId())
            ->setFrom('noreply@job-board.com')
            ->setTo($moderator->getEmail())
            ->setBody($mailBody)
        ;
    }
    
    /**
     * 
     * @param Job $job
     * @param UserInterface $moderator
     * @return string
     */
    private function renderModeratorTemplate(Job $job, UserInterface $moderator)
    {
        return $this->twig->render(
            'AppBundle:job:moderator_email.html.twig',
            array(
                'job' => $job,
                'moderator' => $moderator
            )
        );
    }
} 

