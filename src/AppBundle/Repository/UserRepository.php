<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function findByRole($role) 
    {
        $qb = $this->createQueryBuilder()
            ->select('u')
            ->from('AppBundle:User', 'u')
            ->where('u.roles LIKE :role')
            ->setParameter('role', '%"'.$role.'"%')
        ;

        return $qb->getQuery()->getResult();
    }
}