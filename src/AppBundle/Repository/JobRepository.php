<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class JobRepository extends EntityRepository
{
    /**
     * 
     * @param string $authorEmail
     */
    public function findCountByAuthorEmail($authorEmail) 
    {
        return $this->createQueryBuilder()
                    ->select('COUNT(j.id)') 
                    ->from('AppBundle:Job', 'j')
                    ->where('j.authorEmail = :authorEmail')
                    ->setParameter('authorEmail', $authorEmail)
                    ->getQuery()
                    ->getSingleScalarResult()
                ;
                
    }
}
