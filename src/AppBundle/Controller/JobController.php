<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/job")
 */
class JobController extends Controller 
{
    /**
     * @Route("/", name="job_index")
     */
    public function indexAction() 
    {
        return $this->render('job/index.html.twig');
    }
}
