<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Job;
use AppBundle\Entity\User;
use AppBundle\Event\FirstJobPostedEvent;

/**
 * @Route("/admin/job")
 */
class JobController extends Controller 
{
    /**
     * Lists all job posts
     * 
     * @Route("/", name="admin_job_home")
     */
    public function indexAction() 
    {
        return $this->render('admin/job/index.html.twig');
    }
    
    /**
     * Creates a new Job entity.
     *
     * @Route("/new", name="admin_job_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) 
    {
        $job = new Job();
        $author = $this->getUser();
        $job->setAuthorEmail($author->getEmail());
        
        $form = $this->createForm('AppBundle\Form\JobType', $job);
                
        $form->handleRequest($request);

        if ($form->isValid()) {
            $firstJobPost = $this->isFirstPost($author);
            $entityManager = $this->getDoctrine()->getManager();
            
            $job->setSlug($this->get('app_utility')->slugify($job->getTitle()));
            $job->setPublic(!$firstJobPost);
            
            $entityManager->persist($job);
            $entityManager->flush();
            
            if ($firstJobPost) {
                $this->notifyAuthor($author, $job);
                $this->dispatchFirstJobPostedEvent($job);
            }
            
            $this->addFlash('success', 'Job successfully posted.');

            return $this->redirectToRoute('admin_job_home');
        }

        return $this->render('admin/job/edit.html.twig', array(
            'job' => $job,
            'form' => $form->createView()
        ));
    }
    
    /**
     * Approve (publish) posted Job
     * 
     * @Route("/approve/{id}", requirements={"id": "\d+"}, name="admin_job_approve")
     * @Method("PUT")
     * @Security("has_role('ROLE_MODERATOR')")
     */
    public function approveAction(Job $job) 
    {
        $form = $this->createForm('AppBundle\Form\JobModeratedType', $job);
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $job->setPublic(true);
            $entityManager->flush();
        
            $this->addFlash('success', 'Job successfully approved.');
        } else {
            $this->addFlash('error', 'Something went wrong.');
        }
        
        return $this->redirectToRoute('admin_job_home');
    }
    
    /**
     * Mark Job post as spam
     * 
     * @Route("/mark-spam/{id}", requirements={"id": "\d+"}, name="admin_job_mark_spam")
     * @Method("PUT")
     * @Security("has_role('ROLE_MODERATOR')")
     */
    public function markSpamAction(Job $job) 
    {
        $form = $this->createForm('AppBundle\Form\JobModeratedType', $job);
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $job->setSpam(true);
            $entityManager->flush();
        
            $this->addFlash('success', 'Job successfully approved.');
        } else {
            $this->addFlash('error', 'Something went wrong.');
        }

        return $this->redirectToRoute('admin_job_home');
    }
    
    /**
     * Checks if author has existing job posts
     * 
     * @param User $author
     * @return boolean
     */
    private function isFirstPost(User $author) 
    {
        $authorsJobCount = $this->getDoctrine()->getRepository('AppBundle:Job')->findCountByAuthorEmail($author->getEmail());
        
        return empty($authorsJobCount);
    }
    
    /**
     * Dispatches FirstJobPostedEvent
     * 
     * @param User $author
     */
    private function dispatchFirstJobPostedEvent(User $author) 
    {
        $firstJobPostedEvent = new FirstJobPostedEvent($job);
        $dispatcher = $this->get('event_dispatcher'); 
        
        $dispatcher->dispatch(FirstJobPostedEvent::NAME, $event);
    }
    
    /**
     * 
     * @param Job $job
     * @param User $author
     */
    private function notifyAuthor(Job $job, User $author) 
    {
        $message =  \Swift_Message::newInstance()
            ->setSubject('Job post notification: ' . $job-getTitle() . '/' . $job->getId())
            ->setFrom('noreply@job-board.com')
            ->setTo($author->getEmail())
            ->setBody('Your first Job post is being moderated.')
        ;
        
        $this->get('mailer')->send($message);
    }
}

